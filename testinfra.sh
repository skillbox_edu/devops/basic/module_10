#!/usr/bin/env sh

# See more at https://github.com/angelo-v/testinfra-docker-example
docker run --rm -t \
  -v $(pwd):/project \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  aveltens/docker-testinfra pytest --verbose ./testinfra/
