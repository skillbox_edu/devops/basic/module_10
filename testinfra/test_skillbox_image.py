import time

import pytest
import subprocess
import testinfra


# See at https://testinfra.readthedocs.io/en/latest/examples.html#test-docker-images
# scope='session' uses the same container for all the tests;
# scope='function' uses a new container per test function.
@pytest.fixture(scope='session')
def host(request):
    image_name = "test_skillbox"

    subprocess.check_call(["docker", "build", "-t", image_name, '.'])
    docker_id = subprocess.check_output(["docker", "run", "-d", image_name]).decode().strip()

    # return a testinfra connection to the container
    yield testinfra.get_host("docker://" + docker_id)

    # teardown
    subprocess.check_call(["docker", "rm", "-f", docker_id])


def test_os_alpine_linux(host):
    """Проверка ОС"""
    os_release_file = host.file("/etc/os-release")
    assert b"alpine linux" in os_release_file.content.lower()


def test_nginx_installed(host):
    """Проверка, установлен ли nginx"""
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert "1.16" in nginx.version


def test_nginx_running(host):
    """Проверка, запущен ли nginx"""
    nginx = host.supervisor("nginx", supervisorctl_conf="/etc/supervisor/conf.d/supervisord.conf")

    # wait nginx running
    time.sleep(2)

    assert nginx.is_running
    assert nginx.status == "RUNNING"
