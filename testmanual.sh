#!/usr/bin/env sh

export SKILLBOX=skillbox
docker build -t $SKILLBOX:$SKILLBOX --no-cache . \
   && (docker stop $SKILLBOX || true) \
   && docker run --rm --name $SKILLBOX -d -p 8080:8080 $SKILLBOX:$SKILLBOX \
   && sleep 2 \
   && curl -I http://localhost:8080/
