# Практическая работа по модулю "Тестирование инфраструктурного кода" курса "DevOps-инженер. Основы"

## Цель практической работы
* встроить тестирование в сборку docker images на базе dockerfile 
* создать базовый yaml для деплоя ПО и протестировать его

## Установка и настройка

1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/basic/module_10.git
   cd module_10
   ```
   
## Задание 1

Для просмотра скриншотов лучше кликать по самому изображению, чтобы оно открылось в браузере, и там увеличивать масштаб.

1. Собираем образ
   ```shell
   docker build -t skillbox:skillbox --no-cache .
   ```
   
   Результат сборки образа
   ![Результат сборки образа](docs/imgs/task-1-docker-build.png)

1. Запускаем авто тест. Если нужных образов нет на машине, то придется подождать, пока docker их скачает - после этого выполнятся тесты.
   ```shell
   ./testinfra.sh
   ```
   
   Результат выполнения тестов `testinfra`

   ![Результат выполнения тестов testinfra](docs/imgs/task-1-testinfra-tests.png)
   
1. Можно также запустить ручной тест. Тест запускает контейнер и выполняет запрос к нему при помощи `curl`
   ```shell
   ./testmanual.sh
   ```
   
   Результат проверки контейнера при помощи `curl`
   ![Результат проверки контейнера при помощи curl](docs/imgs/task-1-docker-container-test.png)
   
   Не забываем удалить созданные во время ручного теста контейнеры и образы
   ```shell
   ./testcleanup.sh
   ```
   
## Задание 2

### Тестирование роли `nginx`

1. Устанавливаем `molecule`
   ```shell
   python3 -m venv ~/pyvenv/module_10
   source ~/pyvenv/module_10/bin/activate
   pip install --upgrade setuptools molecule molecule-docker
   molecule --version
   ```

1. Запускаем тесты
   ```shell
   cd ansible/roles/nginx; molecule test || true; cd ../../..
   ```
   
   Результат выполнения тестов `molecule`. Секция выполнения `verify.yml` выделена красной рамкой.
   ![Результат выполнения тестов molecule](docs/imgs/task-2-molecule-tests.png)

### Развертывание контейнера из задания 1 на удаленных ВМ

1. Устанавливаем [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

1. Устанавливаем [Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
   
1. Устанавливаем [Yandex Cloud CLI](https://cloud.yandex.ru/docs/cli/quickstart#install) и [настраиваем](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart)

1. Инициализируем `terraform`
   ```shell
   terraform -chdir=./terraform init
   ```
   
1. Создаем ВМ на Yandex Cloud
   ```shell
   terraform -chdir=./terraform apply -auto-approve
   ```

   IP-адреса ВМ и балансировщика:

   ![ip-адреса ВМ и балансировщика](docs/imgs/task-2-ip-addresses.png)

1. Указываем ip-адреса nginx-серверов и php-приложения в файле [hosts](ansible/hosts)

1. Развертываем php-приложение и nginx-сервера
   ```shell
   cd ansible && ANSIBLE_CONFIG=ansible.cfg ansible-playbook -vv provision.yml; cd ..
   ```
   
   Доступ через балансировщик:

   ![Доступ через балансировщик](docs/imgs/task-2-nginx-load-balancer.png)

1. Удаляем ресурсы Yandex Cloud
   ```shell
   terraform -chdir=./terraform destroy -auto-approve
   ```

## Полезные материалы

* [Тестирование Ansible с использование Molecule](https://habr.com/ru/post/527454)
* [Проверка ролей Ansible через делегированный драйвер](https://habr.com/ru/post/536892)
* [Реализация делегированного драйвера](https://github.com/ansible-community/molecule/issues/1292)
