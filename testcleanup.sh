#!/usr/bin/env sh

export SKILLBOX=skillbox
(docker stop $SKILLBOX || true) \
   && (docker rmi $SKILLBOX:$SKILLBOX || true)
